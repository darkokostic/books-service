CREATE TABLE author (
  id VARCHAR(36) NOT NULL,
  name VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  bio VARCHAR(255),
  creationdate TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY `uqe_name_email` (name, email)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE book (
  id VARCHAR(36) NOT NULL,
  title VARCHAR(255) NOT NULL,
  isbn VARCHAR(255) NOT NULL,
  author VARCHAR(36) NOT NULL,
  issuer VARCHAR(255) NOT NULL,
  pagesnumber INTEGER NOT NULL,
  yearofissue INTEGER NOT NULL,
  creationdate TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (author) REFERENCES author(id),
  UNIQUE KEY `uqe_isbn` (isbn)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;