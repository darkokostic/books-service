package com.vts.edu.rs.books.service.app.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookRepository extends CrudRepository<BookEntity, String> {

    List<BookEntity> findByTitleContainingIgnoreCase(String bookTitle);
}
