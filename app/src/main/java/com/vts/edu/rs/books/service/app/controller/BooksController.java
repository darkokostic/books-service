package com.vts.edu.rs.books.service.app.controller;

import com.vts.edu.rs.books.service.api.books.BooksApi;
import com.vts.edu.rs.books.service.api.books.model.Book;
import com.vts.edu.rs.books.service.api.books.model.BookResponse;
import com.vts.edu.rs.books.service.api.books.model.BookSearchParameters;
import com.vts.edu.rs.books.service.api.books.model.GetBooksResponse;
import com.vts.edu.rs.books.service.app.service.BookService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BooksController implements BooksApi {

    private final BookService bookService;

    public BooksController(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public ResponseEntity<BookResponse> createBook(@Valid Book book) {
        BookResponse bookResponse = bookService.createBook(book);
        return ResponseEntity.ok(bookResponse);
    }

    @Override
    public ResponseEntity<BookResponse> getBookById(String bookId) {
        BookResponse bookResponse = bookService.getBookById(bookId);
        return ResponseEntity.ok(bookResponse);
    }

    @Override
    public ResponseEntity<GetBooksResponse> getBooks() {
        List<BookResponse> books = bookService.getBooks();
        GetBooksResponse getBooksResponse = new GetBooksResponse();
        getBooksResponse.setBooks(books);
        return ResponseEntity.ok(getBooksResponse);
    }

    @Override
    public ResponseEntity<GetBooksResponse> searchBooks(@Valid BookSearchParameters bookSearchParameters) {
        List<BookResponse> books = bookService.searchBooks(bookSearchParameters);
        GetBooksResponse getBooksResponse = new GetBooksResponse();
        getBooksResponse.setBooks(books);
        return ResponseEntity.ok(getBooksResponse);
    }
}
