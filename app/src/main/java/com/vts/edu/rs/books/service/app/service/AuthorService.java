package com.vts.edu.rs.books.service.app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vts.edu.rs.books.service.api.authors.model.Author;
import com.vts.edu.rs.books.service.api.authors.model.AuthorResponse;
import com.vts.edu.rs.books.service.api.authors.model.AuthorSearchParameters;
import com.vts.edu.rs.books.service.app.exceptions.NotFoundException;
import com.vts.edu.rs.books.service.app.mappers.AuthorMapper;
import com.vts.edu.rs.books.service.app.repository.AuthorEntity;
import com.vts.edu.rs.books.service.app.repository.AuthorRepository;
import com.vts.edu.rs.books.service.app.repository.BookEntity;
import com.vts.edu.rs.books.service.app.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorService.class);

    private final AuthorRepository authorRepository;

    private final BookRepository bookRepository;

    private final AuthorMapper authorMapper;

    private final ObjectMapper objectMapper;

    public AuthorService(AuthorRepository authorRepository, BookRepository bookRepository, AuthorMapper authorMapper, ObjectMapper objectMapper) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.authorMapper = authorMapper;
        this.objectMapper = objectMapper;
    }

    public List<AuthorResponse> searchAuthors(AuthorSearchParameters authorSearchParameters) {
        String authorName = authorSearchParameters.getAuthorName();
        if (authorName != null) {
            LOGGER.info("Started searching for authors by author name: {}...", authorName);
            List<AuthorEntity> authorEntities = authorRepository.findByNameContainingIgnoreCase(authorName);
            return authorEntities.stream().map(authorMapper::mapAuthorModel).collect(Collectors.toList());
        }

        String bookId = authorSearchParameters.getBookId();
        if (bookId != null) {
            LOGGER.info("Started searching for authors by book id: {}...", bookId);
            BookEntity bookEntity = bookRepository.findById(bookId).orElseThrow(() ->
                    new NotFoundException(String.format("The book with the id %s does not exist", bookId)));
            AuthorEntity authorEntity = bookEntity.getAuthor();
            return authorEntity != null ? Collections.singletonList(authorMapper.mapAuthorModel(authorEntity)) : new ArrayList<>();
        }

        return new ArrayList<>();
    }

    public List<AuthorResponse> getAuthors() {
        LOGGER.info("Started getting all authors...");
        Iterable<AuthorEntity> authorsIterator = authorRepository.findAll();
        List<AuthorResponse> authors = new ArrayList<>();
        authorsIterator.iterator().forEachRemaining(authorEntity -> authors.add(authorMapper.mapAuthorModel(authorEntity)));
        return authors;
    }

    @Transactional
    public AuthorResponse createAuthor(Author author) {
        try {
            LOGGER.info("Started creating author: {}...", objectMapper.writeValueAsString(author));
            AuthorEntity authorEntity = authorMapper.mapAuthorEntity(author);
            AuthorEntity savedAuthor = authorRepository.save(authorEntity);
            return authorMapper.mapAuthorModel(savedAuthor);
        } catch (JsonProcessingException e) {
            throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to parse provided author model");
        }
    }

    public AuthorResponse getAuthorById(String authorId) {
        LOGGER.info("Started finding author with id: {}...", authorId);
        AuthorEntity authorEntity = authorRepository.findById(authorId).orElseThrow(() ->
                new NotFoundException(String.format("Author with id %s does not exist", authorId)));
        return authorMapper.mapAuthorModel(authorEntity);
    }
}
