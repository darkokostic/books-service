package com.vts.edu.rs.books.service.app.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AuthorRepository extends CrudRepository<AuthorEntity, String> {

    List<AuthorEntity> findByNameContainingIgnoreCase(String name);

}
