package com.vts.edu.rs.books.service.app.mappers;

import com.vts.edu.rs.books.service.api.authors.model.Author;
import com.vts.edu.rs.books.service.api.authors.model.AuthorResponse;
import com.vts.edu.rs.books.service.app.repository.AuthorEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;

@Mapper(componentModel = "spring")
public interface AuthorMapper {

    AuthorResponse mapAuthorModel(AuthorEntity authorEntity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "creationDate", ignore = true)
    AuthorEntity mapAuthorEntity(Author author);

    default OffsetDateTime map(@SuppressWarnings("unused") Instant creationDate) {
        return OffsetDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()),
                ZoneId.systemDefault());
    }
}
