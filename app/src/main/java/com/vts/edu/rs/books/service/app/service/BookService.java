package com.vts.edu.rs.books.service.app.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vts.edu.rs.books.service.api.books.model.Book;
import com.vts.edu.rs.books.service.api.books.model.BookResponse;
import com.vts.edu.rs.books.service.api.books.model.BookSearchParameters;
import com.vts.edu.rs.books.service.app.exceptions.BadRequestException;
import com.vts.edu.rs.books.service.app.exceptions.NotFoundException;
import com.vts.edu.rs.books.service.app.mappers.BookMapper;
import com.vts.edu.rs.books.service.app.repository.AuthorEntity;
import com.vts.edu.rs.books.service.app.repository.AuthorRepository;
import com.vts.edu.rs.books.service.app.repository.BookEntity;
import com.vts.edu.rs.books.service.app.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookService.class);

    private final BookRepository bookRepository;

    private final AuthorRepository authorRepository;

    private final BookMapper bookMapper;

    private final ObjectMapper objectMapper;

    public BookService(BookRepository bookRepository, AuthorRepository authorRepository, BookMapper bookMapper, ObjectMapper objectMapper) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
        this.bookMapper = bookMapper;
        this.objectMapper = objectMapper;
    }

    @Transactional
    public BookResponse createBook(Book book) {
        try {
            LOGGER.info("Started creating book: {}...", objectMapper.writeValueAsString(book));
            String authorId = book.getAuthorId();
            AuthorEntity authorEntity = authorRepository.findById(authorId).orElseThrow(() ->
                    new BadRequestException(String.format("Wrong author ID. The author with id %s not found", authorId)));

            BookEntity bookEntity = bookMapper.mapBookEntity(book);
            bookEntity.setAuthor(authorEntity);

            BookEntity savedBook = bookRepository.save(bookEntity);
            return bookMapper.mapBookModel(savedBook);
        } catch (JsonProcessingException e) {
            throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "Failed to parse provided book model");
        }
    }

    public BookResponse getBookById(String bookId) {
        LOGGER.info("Started finding book with id: {}...", bookId);
        BookEntity bookEntity = bookRepository.findById(bookId).orElseThrow(() ->
                new NotFoundException(String.format("The book with id %s not found", bookId)));
        return bookMapper.mapBookModel(bookEntity);
    }

    public List<BookResponse> getBooks() {
        LOGGER.info("Started getting all books...");
        Iterable<BookEntity> booksIterator = bookRepository.findAll();
        List<BookResponse> books = new ArrayList<>();
        booksIterator.iterator().forEachRemaining(bookEntity -> books.add(bookMapper.mapBookModel(bookEntity)));
        return books;
    }

    public List<BookResponse> searchBooks(BookSearchParameters bookSearchParameters) {
        String bookTitle = bookSearchParameters.getBookTitle();
        if (bookTitle != null) {
            LOGGER.info("Started searching for books by book title: {}...", bookTitle);
            List<BookEntity> bookEntities = bookRepository.findByTitleContainingIgnoreCase(bookTitle);
            return bookEntities.stream().map(bookMapper::mapBookModel).collect(Collectors.toList());
        }

        String authorId = bookSearchParameters.getAuthorId();
        if (authorId != null) {
            LOGGER.info("Started searching for books by author with id: {}...", authorId);
            AuthorEntity authorEntity = authorRepository.findById(authorId).orElseThrow(() ->
                    new NotFoundException(String.format("The author with the id %s does not exist", authorId)));
            return authorEntity.getBooks() == null ? new ArrayList<>() : authorEntity.getBooks()
                    .stream()
                    .map(bookMapper::mapBookModel)
                    .collect(Collectors.toList());
        }

        return new ArrayList<>();
    }
}
