package com.vts.edu.rs.books.service.app.controller;

import com.vts.edu.rs.books.service.api.authors.AuthorsApi;
import com.vts.edu.rs.books.service.api.authors.model.Author;
import com.vts.edu.rs.books.service.api.authors.model.AuthorResponse;
import com.vts.edu.rs.books.service.api.authors.model.AuthorSearchParameters;
import com.vts.edu.rs.books.service.api.authors.model.GetAuthorsResponse;
import com.vts.edu.rs.books.service.app.service.AuthorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class AuthorsController implements AuthorsApi {

    private final AuthorService authorService;

    public AuthorsController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @Override
    public ResponseEntity<GetAuthorsResponse> searchAuthors(@Valid AuthorSearchParameters authorSearchParameters) {
        List<AuthorResponse> authors = authorService.searchAuthors(authorSearchParameters);
        GetAuthorsResponse authorsResponse = new GetAuthorsResponse();
        authorsResponse.setAuthors(authors);
        return ResponseEntity.ok(authorsResponse);
    }

    @Override
    public ResponseEntity<GetAuthorsResponse> getAuthors() {
        List<AuthorResponse> authors = authorService.getAuthors();
        GetAuthorsResponse authorsResponse = new GetAuthorsResponse();
        authorsResponse.setAuthors(authors);
        return ResponseEntity.ok(authorsResponse);
    }

    @Override
    public ResponseEntity<AuthorResponse> createAuthor(@Valid Author author) {
        AuthorResponse createdAuthor = authorService.createAuthor(author);
        return ResponseEntity.ok(createdAuthor);
    }

    @Override
    public ResponseEntity<AuthorResponse> getAuthorById(String authorId) {
        AuthorResponse author = authorService.getAuthorById(authorId);
        return ResponseEntity.ok(author);
    }
}
