package com.vts.edu.rs.books.service.app.mappers;

import com.vts.edu.rs.books.service.api.books.model.Book;
import com.vts.edu.rs.books.service.api.books.model.BookResponse;
import com.vts.edu.rs.books.service.app.repository.BookEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = { AuthorMapper.class })
public interface BookMapper {

    @Mapping(target = "author", ignore = true)
    BookEntity mapBookEntity(Book book);

    BookResponse mapBookModel(BookEntity bookEntity);
}
