package com.vts.edu.rs.books.service.app.support;

import com.vts.edu.rs.books.service.api.authors.model.Author;
import com.vts.edu.rs.books.service.api.books.model.AuthorResponse;
import com.vts.edu.rs.books.service.api.books.model.Book;
import com.vts.edu.rs.books.service.api.books.model.BookResponse;
import com.vts.edu.rs.books.service.app.repository.AuthorEntity;
import com.vts.edu.rs.books.service.app.repository.AuthorRepository;
import com.vts.edu.rs.books.service.app.repository.BookEntity;
import com.vts.edu.rs.books.service.app.repository.BookRepository;
import com.vts.edu.rs.books.service.app.service.AuthorService;
import com.vts.edu.rs.books.service.app.service.BookService;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = AbstractBooksServiceTestCase.Initializer.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@ActiveProfiles("test")
public abstract class AbstractBooksServiceTestCase {

	public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		@Override
		public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
			TestPropertyValues.of(

			).applyTo(configurableApplicationContext.getEnvironment());
		}
	}

	@Autowired
	protected AuthorRepository authorRepository;

	@Autowired
	protected BookRepository bookRepository;

	@Autowired
	protected AuthorService authorService;

	@Autowired
	protected BookService bookService;

	@Before
	public void setup() {
		bookRepository.deleteAll();
		authorRepository.deleteAll();
	}

	@SuppressWarnings("SameParameterValue")
	protected AuthorEntity saveAuthorEntity(String name, String email, String bio) {
		AuthorEntity authorEntity = new AuthorEntity();
		authorEntity.setName(name);
		authorEntity.setEmail(email);
		authorEntity.setBio(bio);
		authorEntity.setCreationDate(Instant.ofEpochMilli(System.currentTimeMillis()));
		return authorRepository.save(authorEntity);
	}

	@SuppressWarnings("SameParameterValue")
	protected Author createAuthorModel(String name, String email, String bio) {
		return new Author()
				.name(name)
				.email(email)
				.bio(bio);
	}

	@SuppressWarnings("SameParameterValue")
	protected Book createBookModel(String title, String isbn, String issuer, int pagesNumber, int year, String authorEntityId) {
		return new Book()
				.title(title)
				.isbn(isbn)
				.issuer(issuer)
				.pagesNumber(pagesNumber)
				.yearOfIssue(year)
				.authorId(authorEntityId);
	}

	@SuppressWarnings("SameParameterValue")
	protected BookEntity saveBookEntity(String title, String isbn, String issuer, int pagesNumber, int year, AuthorEntity author) {
		BookEntity bookEntity = new BookEntity();
		bookEntity.setTitle(title);
		bookEntity.setIsbn(isbn);
		bookEntity.setIssuer(issuer);
		bookEntity.setPagesNumber(pagesNumber);
		bookEntity.setYearOfIssue(year);
		bookEntity.setAuthor(author);
		return bookRepository.save(bookEntity);
	}

	protected void assertAuthor(AuthorEntity authorEntity, AuthorResponse authorResponse) {
		assertNotNull(authorResponse);
		assertEquals(authorEntity.getName(), authorResponse.getName());
		assertEquals(authorEntity.getEmail(), authorResponse.getEmail());
		assertEquals(authorEntity.getBio(), authorResponse.getBio());
	}

	protected void assertAuthor(Author author, com.vts.edu.rs.books.service.api.authors.model.AuthorResponse authorResponse) {
		assertNotNull(authorResponse);
		assertEquals(author.getName(), authorResponse.getName());
		assertEquals(author.getEmail(), authorResponse.getEmail());
		assertEquals(author.getBio(), authorResponse.getBio());
	}

	protected void assertAuthor(AuthorEntity authorEntity, com.vts.edu.rs.books.service.api.authors.model.AuthorResponse authorResponse) {
		assertNotNull(authorResponse);
		assertEquals(authorEntity.getName(), authorResponse.getName());
		assertEquals(authorEntity.getEmail(), authorResponse.getEmail());
		assertEquals(authorEntity.getBio(), authorResponse.getBio());
	}

	protected void assertBook(BookResponse bookResponse, BookEntity bookEntity) {
		assertNotNull(bookResponse);
		assertEquals(bookEntity.getTitle(), bookResponse.getTitle());
		assertEquals(bookEntity.getIsbn(), bookResponse.getIsbn());
		assertEquals(bookEntity.getIssuer(), bookResponse.getIssuer());
		assertEquals(bookEntity.getPagesNumber(), bookResponse.getPagesNumber());
		assertEquals(bookEntity.getYearOfIssue(), bookResponse.getYearOfIssue());
	}

	protected void assertBook(BookResponse bookResponse, Book book) {
		assertNotNull(bookResponse);
		assertEquals(book.getTitle(), bookResponse.getTitle());
		assertEquals(book.getIsbn(), bookResponse.getIsbn());
		assertEquals(book.getIssuer(), bookResponse.getIssuer());
		assertEquals(book.getPagesNumber(), bookResponse.getPagesNumber());
		assertEquals(book.getYearOfIssue(), bookResponse.getYearOfIssue());
	}

}
