package com.vts.edu.rs.books.service.app.service;

import com.vts.edu.rs.books.service.api.books.model.AuthorResponse;
import com.vts.edu.rs.books.service.api.books.model.Book;
import com.vts.edu.rs.books.service.api.books.model.BookResponse;
import com.vts.edu.rs.books.service.api.books.model.BookSearchParameters;
import com.vts.edu.rs.books.service.app.exceptions.BadRequestException;
import com.vts.edu.rs.books.service.app.repository.AuthorEntity;
import com.vts.edu.rs.books.service.app.repository.BookEntity;
import com.vts.edu.rs.books.service.app.support.AbstractBooksServiceTestCase;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class BookServiceTest extends AbstractBooksServiceTestCase {

    @Test
    public void createBookWorksAsExpected() {
        AuthorEntity authorEntity = saveAuthorEntity("Joe Doe", "author@demo.com", "Bio");

        Book book = createBookModel("Title", "isbn", "Issuer", 100, 2020,
                authorEntity.getId());
        BookResponse bookResponse = bookService.createBook(book);

        assertBook(bookResponse, book);

        AuthorResponse retrievedAuthor = bookResponse.getAuthor();
        assertAuthor(authorEntity, retrievedAuthor);
    }

    @Test(expected = BadRequestException.class)
    public void createBookWithWrongAuthorIdThrowsException() {
        Book book = createBookModel("Title", "isbn", "Issuer", 100, 2020,
                "wrong-author-id");
        bookService.createBook(book);
    }

    @Test
    public void getBooksWorksAsExpected() {
        AuthorEntity authorEntity = saveAuthorEntity("Joe Doe", "author@demo.com", "Bio");
        BookEntity bookEntity = saveBookEntity("Title", "isbn", "Issuer", 200, 202, authorEntity);

        List<BookResponse> books = bookService.getBooks();
        assertFalse(books.isEmpty());

        BookResponse bookResponse = books.get(0);
        assertBook(bookResponse, bookEntity);
    }

    @Test
    public void getBookByIdWorksAsExpected() {
        AuthorEntity authorEntity = saveAuthorEntity("Joe Doe", "author@demo.com", "Bio");
        BookEntity bookEntity = saveBookEntity("Title", "isbn", "Issuer", 200, 202, authorEntity);

        BookResponse bookResponse = bookService.getBookById(bookEntity.getId());
        assertBook(bookResponse, bookEntity);
    }

    @Test
    public void searchBooksByTitleWorksAsExpected() {
        AuthorEntity authorEntity = saveAuthorEntity("Joe Doe", "author@demo.com", "Bio");
        BookEntity bookEntity = saveBookEntity("Title", "isbn", "Issuer", 200, 202, authorEntity);

        BookSearchParameters bookSearchParameters = new BookSearchParameters();
        bookSearchParameters.setBookTitle(bookEntity.getTitle());
        List<BookResponse> books = bookService.searchBooks(bookSearchParameters);
        assertFalse(books.isEmpty());

        BookResponse bookResponse = books.get(0);
        assertBook(bookResponse, bookEntity);
    }
}
