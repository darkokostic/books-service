package com.vts.edu.rs.books.service.app.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DatabaseTest {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private BookRepository bookRepository;

    @Before
    public void setup() {
        authorRepository.deleteAll();
        bookRepository.deleteAll();
    }

    @Test
    public void createAuthor() {
        saveAuthorEntity();

        List<AuthorEntity> foundEntities = new ArrayList<>();
        authorRepository.findAll().iterator().forEachRemaining(foundEntities::add);
        assertEquals("Wrong number of authors found in database. The author is not saved.", 1, foundEntities.size());
    }

    @Test
    public void createBook() {
        AuthorEntity authorEntity = saveAuthorEntity();

        BookEntity bookEntity = new BookEntity();
        bookEntity.setTitle("Book Title");
        bookEntity.setIsbn(UUID.randomUUID().toString());
        bookEntity.setAuthor(authorEntity);
        bookEntity.setIssuer("Issuer");
        bookEntity.setPagesNumber(570);
        bookEntity.setYearOfIssue(1972);
        bookRepository.save(bookEntity);

        List<BookEntity> foundEntities = new ArrayList<>();
        bookRepository.findAll().iterator().forEachRemaining(foundEntities::add);
        assertEquals("Wrong number of books found in database. The book is not saved.", 1, foundEntities.size());
    }

    private AuthorEntity saveAuthorEntity() {
        AuthorEntity authorEntity = new AuthorEntity();
        authorEntity.setName("John Doe");
        authorEntity.setEmail("johndoe@demo.com");
        authorEntity.setBio("John Doe Bio");
        authorRepository.save(authorEntity);
        return authorEntity;
    }
}
