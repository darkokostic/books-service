package com.vts.edu.rs.books.service.app.service;

import com.vts.edu.rs.books.service.api.authors.model.Author;
import com.vts.edu.rs.books.service.api.authors.model.AuthorResponse;
import com.vts.edu.rs.books.service.api.authors.model.AuthorSearchParameters;
import com.vts.edu.rs.books.service.app.exceptions.NotFoundException;
import com.vts.edu.rs.books.service.app.repository.AuthorEntity;
import com.vts.edu.rs.books.service.app.support.AbstractBooksServiceTestCase;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class AuthorServiceTest extends AbstractBooksServiceTestCase {

    @Test
    public void getAuthorsWorksAsExpected() {
        AuthorEntity authorEntity = saveAuthorEntity("Joe Doe", "author@demo.com", "Bio");

        List<AuthorResponse> authors = authorService.getAuthors();
        assertFalse(authors.isEmpty());

        AuthorResponse authorResponse = authors.get(0);
        assertAuthor(authorEntity, authorResponse);
    }

    @Test
    public void getAuthorByIdWorksAsExpected() {
        AuthorEntity authorEntity = saveAuthorEntity("Joe Doe", "author@demo.com", "Bio");

        AuthorResponse authorResponse = authorService.getAuthorById(authorEntity.getId());
        assertAuthor(authorEntity, authorResponse);
    }

    @Test(expected = NotFoundException.class)
    public void getAuthorByIdWithWrongIdThrowsException() {
        authorService.getAuthorById("wrong-author-id");
    }

    @Test
    public void createAuthorWorksAsExpected() {
        Author author = createAuthorModel("Joe Doe", "author@demo.com", "Bio");
        AuthorResponse authorResponse = authorService.createAuthor(author);
        assertAuthor(author, authorResponse);
    }

    @Test
    public void searchAuthorsByNameWorksAsExpected() {
        AuthorEntity authorEntity = saveAuthorEntity("Joe Doe", "author@demo.com", "Bio");

        AuthorSearchParameters authorsSearchParameters = new AuthorSearchParameters();
        authorsSearchParameters.setAuthorName(authorEntity.getName());
        List<AuthorResponse> authors = authorService.searchAuthors(authorsSearchParameters);
        assertFalse(authors.isEmpty());

        AuthorResponse authorResponse = authors.get(0);
        assertAuthor(authorEntity, authorResponse);
    }
}
