package com.vts.edu.rs.books.service.client;

import com.vts.edu.rs.books.service.api.authors.model.Author;
import com.vts.edu.rs.books.service.api.authors.model.AuthorResponse;
import com.vts.edu.rs.books.service.api.authors.model.AuthorSearchParameters;
import com.vts.edu.rs.books.service.api.authors.model.GetAuthorsResponse;
import com.vts.edu.rs.books.service.api.books.model.Book;
import com.vts.edu.rs.books.service.api.books.model.BookResponse;
import com.vts.edu.rs.books.service.api.books.model.BookSearchParameters;
import com.vts.edu.rs.books.service.api.books.model.GetBooksResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class BooksServiceRestClient implements BooksServiceClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(BooksServiceRestClient.class);

    private final String booksServiceBaseUrl;

    private final RestTemplate restTemplate;

    public BooksServiceRestClient(String booksServiceBaseUrl, RestTemplate restTemplate) {
        this.booksServiceBaseUrl = booksServiceBaseUrl;
        this.restTemplate = restTemplate;
    }

    @Override
    public AuthorResponse getAuthorById(String authorId) throws BooksServiceClientException {
        LOGGER.info("Calling book-service to find author by id: {}", authorId);

        String url = booksServiceBaseUrl + "/authors/{authorId}";

        try {
            AuthorResponse response = restTemplate.getForObject(url, AuthorResponse.class, authorId);
            LOGGER.info("Retrieved author by id: {}", response);
            return response;
        } catch (RestClientException e) {
            LOGGER.info("Getting author failed", e);
            throw new BooksServiceClientException(e);
        }
    }

    @Override
    public List<AuthorResponse> getAuthors() throws BooksServiceClientException {
        LOGGER.info("Calling book-service to get all authors");

        String url = booksServiceBaseUrl + "/authors";

        try {
            GetAuthorsResponse response = restTemplate.getForObject(url, GetAuthorsResponse.class);
            LOGGER.trace("Retrieved all authors: {}", response);
            if (response != null) {
                return response.getAuthors();
            }
            return new ArrayList<>();
        } catch (RestClientException e) {
            LOGGER.debug("Getting all authors failed", e);
            throw new BooksServiceClientException(e);
        }
    }

    @Override
    public List<AuthorResponse> searchAuthors(AuthorSearchParameters authorSearchParameters) throws BooksServiceClientException {
        LOGGER.info("Calling book-service to search for authors");

        if (authorSearchParameters == null) {
            authorSearchParameters = new AuthorSearchParameters();
        }

        String url = booksServiceBaseUrl + "/authors/search";

        try {
            HttpEntity<AuthorSearchParameters> httpEntity = new HttpEntity<>(authorSearchParameters, new HttpHeaders());
            ResponseEntity<GetAuthorsResponse> responseEntity = restTemplate.exchange(url,
                    HttpMethod.POST,
                    httpEntity,
                    GetAuthorsResponse.class);
            GetAuthorsResponse response = responseEntity.getBody();
            LOGGER.trace("Retrieved authors: {}", response);
            if (response != null) {
                return response.getAuthors();
            }
            return new ArrayList<>();
        } catch (RestClientException e) {
            LOGGER.debug("Getting all authors failed", e);
            throw new BooksServiceClientException(e);
        }
    }

    @Override
    public AuthorResponse createAuthor(@Valid Author author) throws BooksServiceClientException {
        LOGGER.info("Calling book-service to create author");

        String url = booksServiceBaseUrl + "/authors";

        try {
            AuthorResponse response = restTemplate.postForObject(url,
                    author,
                    AuthorResponse.class);
            LOGGER.trace("Created author: {}", response);
            return response;
        } catch (RestClientException e) {
            LOGGER.info("Creating author failed", e);
            throw new BooksServiceClientException(e);
        }
    }

    @Override
    public BookResponse createBook(@Valid Book book) throws BooksServiceClientException {
        LOGGER.info("Calling book-service to create book");

        String url = booksServiceBaseUrl + "/books";

        try {
            BookResponse response = restTemplate.postForObject(url,
                    book,
                    BookResponse.class);
            LOGGER.trace("Created book: {}", response);
            return response;
        } catch (RestClientException e) {
            LOGGER.info("Creating book failed", e);
            throw new BooksServiceClientException(e);
        }
    }

    @Override
    public BookResponse getBookById(String bookId) throws BooksServiceClientException {
        LOGGER.info("Calling book-service to find book by id: {}", bookId);

        String url = booksServiceBaseUrl + "/books/{bookId}";

        try {
            BookResponse response = restTemplate.getForObject(url, BookResponse.class, bookId);
            LOGGER.info("Retrieved book by id: {}", response);
            return response;
        } catch (RestClientException e) {
            LOGGER.info("Getting book failed", e);
            throw new BooksServiceClientException(e);
        }
    }

    @Override
    public GetBooksResponse getBooks() throws BooksServiceClientException {
        LOGGER.info("Calling book-service to get all books");

        String url = booksServiceBaseUrl + "/books";

        try {
            GetBooksResponse response = restTemplate.getForObject(url, GetBooksResponse.class);
            LOGGER.trace("Retrieved all books: {}", response);
            return response;
        } catch (RestClientException e) {
            LOGGER.debug("Getting all books failed", e);
            throw new BooksServiceClientException(e);
        }
    }

    @Override
    public GetBooksResponse searchBooks(BookSearchParameters bookSearchParameters) throws BooksServiceClientException {
        LOGGER.info("Calling book-service to search for books");

        String url = booksServiceBaseUrl + "/books/search";

        try {
            HttpEntity<BookSearchParameters> httpEntity = new HttpEntity<>(bookSearchParameters, new HttpHeaders());
            ResponseEntity<GetBooksResponse> responseEntity = restTemplate.exchange(url,
                    HttpMethod.POST,
                    httpEntity,
                    GetBooksResponse.class);
            GetBooksResponse response = responseEntity.getBody();
            LOGGER.trace("Retrieved books: {}", response);
            return response;
        } catch (RestClientException e) {
            LOGGER.debug("Searching books failed", e);
            throw new BooksServiceClientException(e);
        }
    }
}
