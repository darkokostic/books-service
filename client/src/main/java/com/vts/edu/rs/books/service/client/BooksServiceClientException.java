package com.vts.edu.rs.books.service.client;

public class BooksServiceClientException extends Exception {

    public BooksServiceClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public BooksServiceClientException(Throwable cause) {
        super(cause);
    }
}
