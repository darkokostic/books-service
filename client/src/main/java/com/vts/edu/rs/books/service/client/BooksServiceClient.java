package com.vts.edu.rs.books.service.client;

import com.vts.edu.rs.books.service.api.authors.model.Author;
import com.vts.edu.rs.books.service.api.authors.model.AuthorResponse;
import com.vts.edu.rs.books.service.api.authors.model.AuthorSearchParameters;
import com.vts.edu.rs.books.service.api.books.model.Book;
import com.vts.edu.rs.books.service.api.books.model.BookResponse;
import com.vts.edu.rs.books.service.api.books.model.BookSearchParameters;
import com.vts.edu.rs.books.service.api.books.model.GetBooksResponse;

import javax.validation.Valid;
import java.util.List;

public interface BooksServiceClient {

    AuthorResponse createAuthor(@Valid Author author) throws BooksServiceClientException;

    AuthorResponse getAuthorById(String authorId) throws BooksServiceClientException;

    List<AuthorResponse> getAuthors() throws BooksServiceClientException;

    List<AuthorResponse> searchAuthors(AuthorSearchParameters authorSearchParameters) throws BooksServiceClientException;

    BookResponse createBook(@Valid Book book) throws BooksServiceClientException;

    BookResponse getBookById(String bookId) throws BooksServiceClientException;

    GetBooksResponse getBooks() throws BooksServiceClientException;

    GetBooksResponse searchBooks(BookSearchParameters bookSearchParameters) throws BooksServiceClientException;
}
