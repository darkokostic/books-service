package com.vts.edu.rs.books.service.app.test;

import com.vts.edu.rs.books.service.api.authors.model.Author;
import com.vts.edu.rs.books.service.api.authors.model.AuthorResponse;
import com.vts.edu.rs.books.service.api.authors.model.AuthorSearchParameters;
import com.vts.edu.rs.books.service.api.books.model.Book;
import com.vts.edu.rs.books.service.api.books.model.BookResponse;
import com.vts.edu.rs.books.service.api.books.model.BookSearchParameters;
import com.vts.edu.rs.books.service.api.books.model.GetBooksResponse;
import com.vts.edu.rs.books.service.client.BooksServiceClient;
import com.vts.edu.rs.books.service.client.BooksServiceRestClient;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.containers.DockerComposeContainer;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

public class BooksServiceIT {

    private static final String TEST_COMPOSE_FILE_URL = "src/test/resources/test-compose.yml";

    private static final String BOOKS_SERVICE_NAME = "books-service";

    private static final String BOOK_SERVICE_BASE_URL = "http://localhost:8888";

    @ClassRule
    public static DockerComposeContainer DOCKER_COMPOSE_SETUP = new DockerComposeContainer(
                new File(TEST_COMPOSE_FILE_URL))
                .withLocalCompose(true)
                .withExposedService(BOOKS_SERVICE_NAME, 8080);

    private BooksServiceClient booksServiceClient;

    @Before
    public void setup() {
        booksServiceClient = createBookServiceClient();
    }

    @Test
    public void createBookWorksAsExpected() throws Exception {
        Author author = createAuthorModel("Author 1", "author1@demo.com");
        AuthorResponse authorResponse = booksServiceClient.createAuthor(author);

        Book book = createBookModel(authorResponse.getId(), "isbn1");
        BookResponse bookResponse = booksServiceClient.createBook(book);
        assertBook(book, bookResponse, authorResponse.getId());
    }

    @Test
    public void getBooksWorksAsExpected() throws Exception {
        Author author = createAuthorModel("Author 2", "author2@demo.com");
        AuthorResponse authorResponse = booksServiceClient.createAuthor(author);

        Book book = createBookModel(authorResponse.getId(), "isbn2");
        booksServiceClient.createBook(book);

        GetBooksResponse response = booksServiceClient.getBooks();
        List<BookResponse> books = response.getBooks();
        assertFalse(books.isEmpty());
    }

    @Test
    public void getBookByIdWorksAsExpected() throws Exception {
        Author author = createAuthorModel("Author 3", "author3@demo.com");
        AuthorResponse authorResponse = booksServiceClient.createAuthor(author);

        Book book = createBookModel(authorResponse.getId(), "isbn3");
        BookResponse createdBook = booksServiceClient.createBook(book);

        BookResponse bookResponse = booksServiceClient.getBookById(createdBook.getId());
        assertBook(book, bookResponse, authorResponse.getId());
    }

    @Test
    public void searchBooksByTitle() throws Exception {
        Author author = createAuthorModel("Author 4", "author4@demo.com");
        AuthorResponse authorResponse = booksServiceClient.createAuthor(author);

        Book book = createBookModel(authorResponse.getId(), "isbn4");
        BookResponse bookResponse = booksServiceClient.createBook(book);

        BookSearchParameters booksSearchParameters = new BookSearchParameters();
        booksSearchParameters.setBookTitle(bookResponse.getTitle());
        GetBooksResponse response = booksServiceClient.searchBooks(booksSearchParameters);
        List<BookResponse> books = response.getBooks();
        assertFalse(books.isEmpty());
    }

    @Test
    public void createAuthorWorksAsExpected() throws Exception {
        Author author = createAuthorModel("Author 5", "author5@demo.com");
        AuthorResponse authorResponse = booksServiceClient.createAuthor(author);
        assertAuthor(author, authorResponse);
    }

    @Test
    public void getAuthorsWorksAsExpected() throws Exception {
        Author author = createAuthorModel("Author 6", "author6@demo.com");
        booksServiceClient.createAuthor(author);

        List<AuthorResponse> authors = booksServiceClient.getAuthors();
        assertFalse(authors.isEmpty());
    }

    @Test
    public void getAuthorByIdWorksAsExpected() throws Exception {
        Author author = createAuthorModel("Author 7", "author7@demo.com");
        AuthorResponse createdAuthor = booksServiceClient.createAuthor(author);

        AuthorResponse authorResponse = booksServiceClient.getAuthorById(createdAuthor.getId());
        assertAuthor(author, authorResponse);
    }

    @Test
    public void searchAuthorsByName() throws Exception {
        Author author = createAuthorModel("Author 8", "author8@demo.com");
        AuthorResponse createdAuthor = booksServiceClient.createAuthor(author);

        AuthorSearchParameters authorSearchParameters = new AuthorSearchParameters();
        authorSearchParameters.setAuthorName(createdAuthor.getName());
        List<AuthorResponse> authorResponses = booksServiceClient.searchAuthors(authorSearchParameters);
        assertFalse(authorResponses.isEmpty());
    }

    private BooksServiceClient createBookServiceClient() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestTemplate restTemplate = restTemplateBuilder.build();
        return new BooksServiceRestClient(BOOK_SERVICE_BASE_URL, restTemplate);
    }

    private Author createAuthorModel(String name, String email) {
        Author author = new Author();
        author.setName(name);
        author.setEmail(email);
        author.setBio("Bio");
        return author;
    }

    private Book createBookModel(String authorId, String isbn) {
        Book book = new Book();
        book.setTitle("Book1");
        book.setIsbn(isbn);
        book.setIssuer("Issuer");
        book.setPagesNumber(234);
        book.setYearOfIssue(2007);
        book.setAuthorId(authorId);
        return book;
    }

    private void assertBook(Book book, BookResponse bookResponse, String authorId) {
        assertNotNull(bookResponse);
        assertEquals(book.getTitle(), bookResponse.getTitle());
        assertEquals(book.getIsbn(), bookResponse.getIsbn());
        assertEquals(book.getIssuer(), book.getIssuer());
        assertEquals(book.getPagesNumber(), bookResponse.getPagesNumber());
        assertEquals(book.getYearOfIssue(), bookResponse.getYearOfIssue());
        assertEquals(book.getAuthorId(), authorId);
    }

    private void assertAuthor(Author author, AuthorResponse authorResponse) {
        assertNotNull(authorResponse);
        assertEquals(author.getName(), authorResponse.getName());
        assertEquals(author.getEmail(), authorResponse.getEmail());
        assertEquals(author.getBio(), authorResponse.getBio());
    }
}
